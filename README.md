# README #

This repository contains the binaries installed by a halide mac installer. The original .tgz format of this installer can be found here:
https://github.com/halide/Halide/releases

The date and SHA of the release of the halide install contained in this repo is:
Date: 05/03/2017
SHA: 06ace54101cbd656e22243f86cce0a82ba058c3b
